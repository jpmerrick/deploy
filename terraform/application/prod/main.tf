module "helloworld_application" {
  source = "../../modules/helloworld/"
  env = "prod" 
}

terraform {
  backend "s3" {
    bucket = "test-han-jpm-tfstate"
    key    = "terraform-prod.tfstate"
    region = "eu-west-2"
  }
}
