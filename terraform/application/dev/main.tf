module "helloworld_application" {
  source = "../../modules/helloworld/"
  env = "dev" 
}

terraform {
  backend "s3" {
    bucket = "test-han-jpm-tfstate"
    key    = "terraform-dev.tfstate"
    region = "eu-west-2"
  }
}
