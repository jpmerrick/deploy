module "helloworld_application" {
  source = "../../modules/helloworld/"
  env = "test" 
}

terraform {
  backend "s3" {
    bucket = "test-han-jpm-tfstate"
    key    = "terraform-test.tfstate"
    region = "eu-west-2"
  }
}
