
# We'll use the default VPC for RDS as the application is running in it's own. In practice, this would be it's own VPC or isolated subnet.

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.default.id
}

data "aws_security_group" "default" {
  vpc_id = data.aws_vpc.default.id
  name   = "default"
}


module "db" {
  source = "terraform-aws-modules/rds/aws"

  identifier = "${var.env}-helloworlddb-postgres"

  engine            = "postgres"
  engine_version    = "9.6.18"
  instance_class    = "db.t2.small"
  allocated_storage = 5
  storage_encrypted = false # This would be in reality, unencrypted for simplicity here

  name = "helloworlddb"

  # NOTE: Do NOT use 'user' as the value for 'username' as it throws:
  # "Error creating DB Instance: InvalidParameterValue: MasterUsername
  # user cannot be used as it is a reserved word used by the engine"
  username = "helloworlduser"

  password = "thoo1iecoh2ohJah7lie" # This would be stored more securely in practice
  port     = "5432"

  vpc_security_group_ids = [data.aws_security_group.default.id]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  # disable backups to create DB faster
  backup_retention_period = 0

  tags = {
    Owner       = "user"
    Environment = "${var.env}"
  }

  enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]

  # DB subnet group
  subnet_ids = data.aws_subnet_ids.all.ids

  # DB parameter group
  family = "postgres9.6"

  # DB option group
  major_engine_version = "9.6"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "${var.env}-helloworlddb"

  # Database Deletion Protection
  deletion_protection = false
}
