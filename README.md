
# Overview 

This repository contains the necessary files to do a basic application deployment. This consists of

* Applicaton Code - A simple helloworld application written in Python, using Flask.
* Dockerfile - This builds the container which will be deployed. It utilised the official python upstream image. 
* Gitlab CI - A manifest to run the Gitlab pipeline. This will do a docker build, run tests (unit/lint/e2e/style) and push/tag to Gitlab's container registry. Some steps in this have been simplified for this excercise. There are also some security tests that are run here, to check the dependencies have no advisories.
* Kubernetes Manifest - A simple kubernetes YAML configuration to create a deployment with 2 pods and a load balancer to service the requests. 

```
N.B. There is some hardcoded `jpmerrick` within the various codebases, this would ideally be a variable that could be interpolated in a real-world scenario.
Also, please update the s3 buckets within the terraform calling module (inside main.tf in dev, test, prod directries), this would be dynamic in real-world.
```

# Requirements

* Terraform (Tested with v0.12.16) 
* wget (required for the eks module)
* AWS account (configured for shell access and with appropriate IAM policy for EKS and S3)
* S3 bucket for terraform state
* kubectl 
* Gitlab Account with Personal Access Token (used with the container registry access scopes)

# Configuration

The project needs a masked variable to be set within Gitlab. `$CI_JOB_TOKEN` should be set to the Gitlab personal access token for the account.

# Steps

In order to deploy the application, the following order is needed

Terraform -> Gitlab CI Trigger -> Manual run of `kubectl apply` for the given environment (this would be managed via the CI/CD using a Helm chart and with tags and canary builds in reality)

## Terraform

There are three different kubernetes clusters that can be terraformed `dev, test, prod`. For this example, we will use

* cd terraform/application/dev
* terraform init
* terraform plan
* terraform apply

This will output a kubernetes configuration file, the very last entry in that file can be used to configure kubectl via aws tooling.
i.e. `aws eks --region eu-west-2 update-kubeconfig --name helloworld-dev-jTxmjTaM`

## Gitlab

A change in the codebase, pushed will trigger a build and use a temporary image ID. Tests will also be run (see .gitlab-ci.yml). If these are successful and the branch is `master` then the image is tagged with 'latest'. This allows for feature branches or other test branches to have their own image without causing latest to update.

## Kubernetes

A simple manifest can be applied to create a deployment and load balancer service via EKS. It is important to keep in mind this is scoped for the environment that is created in the terraform step. In practice this would be tagged with more metadata and approriately air gapped etc.

* kubectl apply -f kubernetes/helloworld.yml 

This will build in the default namespace for simplicity. The service endpoint can be checked like so

```
# kubectl get svc/helloworld-service
NAME                 TYPE           CLUSTER-IP       EXTERNAL-IP                                                               PORT(S)        AGE
helloworld-service   LoadBalancer   172.20.249.146   aa6b8c14799fd45ae9b4ddff63494563-2073892589.eu-west-2.elb.amazonaws.com   80:30671/TCP   40s

http://aa6b8c14799fd45ae9b4ddff63494563-2073892589.eu-west-2.elb.amazonaws.com can now be curl/checked via browser

```

# Database

A postgres database is also created by the terraform. This builds within the defalult VPC subnet within the environment. In practice this would be a bit cleaner and have tighter ACLs if needed.

