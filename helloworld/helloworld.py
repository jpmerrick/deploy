"""
This is a simple app using flask to output text on a route
"""
import uuid
from flask import Flask
app = Flask(__name__)

ID = str(uuid.uuid1())


@app.route("/")
def hello():
    """ REST, 90's version """
    output = "<marquee><h1>Hello World!</h1></marquee><h4>ID: " + ID + "</h4>"
    return output


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
