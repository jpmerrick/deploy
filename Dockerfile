# Use the 3.8 version of Python with the minimal Alpine linux disto
FROM python:3.8-alpine

# Add a user so we're not running the code as root
RUN adduser -D helloworld

# Set the working directory
WORKDIR /home/helloworld

# Copy the pip requirements file, entrypoint and code into the container
COPY helloworld/requirements.txt .
COPY helloworld/helloworld.py .

# Run the pip installation inside the container
RUN pip install -r requirements.txt

# Set the runtime user to helloworld
USER helloworld

# Expose port 5000 which helloworld is served on to the host
EXPOSE 5000
# Application Entrypoint. i.e. Would you start the fans please!
ENTRYPOINT ["/usr/local/bin/python","helloworld.py"]
